import http from '@/util/http';

export default {
    //模糊查询，分页
    search(params){
        return http.get('/opportunity/page',params)
        //return http.get('/json/opportunity.json',params);
    },
    //新建或编辑
    saveOrUpdate(params){
        return http.post('/opportunity/saveOrUpdate',params);
    },
    //根据销售机会ID 查询
    findById(id){
        return http.get(`/opportunity/${id}`);
    },
    //查询所有销售主管
    findAllMgr(){
        return http.get('/opportunity/getAllMgr');
    },
    //删除未指派的销售机会
    delById(id){
        return http.del(`/opportunity/${id}`);
    },
    //指派销售机会
    appoint(id,appointId){
        return http.put(`/opportunity/${id}/${appointId}`);
    }
}