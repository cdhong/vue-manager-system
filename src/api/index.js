import user from './user';
import opportunity from './opportunity';

export default{
    user,
    opportunity
}