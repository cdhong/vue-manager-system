import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        collapse:false, //导航折叠菜单状态，默认关闭
        //user:localStorage.getItem('user' || '[]') == null ? {} : JSON.parse(localStorage.getItem('user' || '[]')),
        user:{}, //当前登录的用户信息
        routes: []
    },
    mutations:{
        isCollapse(state){
            state.collapse = !state.collapse;
        },
        setUserInfo(state,userInfo){
            if(userInfo === undefined){
                state.user = {};
                return;
            }
            state.user = userInfo;
        },
        initMenu(state, menus){
            state.routes = menus;
        }
    }
});